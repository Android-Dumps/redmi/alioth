## qssi-user 12
12 SKQ1.220213.001 22.4.13 release-keys
- Manufacturer: xiaomi
- Platform: kona
- Codename: alioth
- Brand: Redmi
- Flavor: qssi-user
- Release Version: 12
12
- Id: SKQ1.220213.001
- Incremental: 22.4.13
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: zh-CN
- Screen Density: 440
- Fingerprint: Redmi/alioth/alioth:12/RKQ1.211001.001/22.4.13:user/release-keys
- OTA version: 
- Branch: qssi-user-12
12-SKQ1.220213.001-22.4.13-release-keys
- Repo: redmi/alioth
